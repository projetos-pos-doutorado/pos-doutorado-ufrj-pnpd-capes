[![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](./LICENSE.md)

Projeto para estágio pós-doutoral no Programa de Pós-Graduação em Matemática do Instituto de Matemática da Universidade Federal do Rio de Janeiro - [PGMAT/IM/UFRJ](http://www.pgmat.im.ufrj.br/), Brasil.

Projeto apresentado ao [PGMAT/IM/UFRJ](http://www.pgmat.im.ufrj.br/), atendendo a [chamada pública](https://mailman.impa.br/pipermail/forumppg/2017-March/002593.html) para inscrições no processo seletivo para posições de Pós-Doutorado através de bolsas do Programa Nacional de Pós-Doutorado - [PNPD/CAPES](http://www.capes.gov.br/bolsas/bolsas-no-pais/pnpd-capes).