\documentclass[12pt]{article}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{textcomp}
\usepackage[brazil]{babel}
\newtheorem{thm}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{lemma}{Lemma}
\newtheorem{question}{Question}
\newtheorem{defn}{Definition}
\newtheorem{aspt}{Assumption}
\newtheorem{conj}{Conjecture}
\newtheorem{cor}{Corollary}
\newtheorem{teorema}{Teorema}
\usepackage{setspace}
\doublespacing
%%%%%
\begin{document}
\pagestyle{empty}
%%%%%
\begin{center}
{\Large PROJETO DE PESQUISA}\\
\vspace*{7cm}
{\LARGE A CONJECTURA DA MASSA POSITIVA \\  E A DESIGUALDADE DE PENROSE}\\
\vspace*{7cm}
{\Large FREDERICO VALE GIR\~AO}\\
{\large Agosto de 2015}
\end{center}
\newpage
\pagestyle{plain}
\singlespacing

\section*{Introdu\c c\~ao}

Relatividade Geral (RG) \'e a teoria do espa\c co, tempo e gravita\c c\~ao introduzida por Einstein em 1915. Nela, ele procura explicar a gravita\c c\~ao como uma consequ\^encia da curvatura do espa\c co-tempo, curvatura esta causada pela distribui\c c\~ao de mat\'eria. 

Diferentemente da Mec\^anica Newtoniana, RG \'e inteiramente consistente com os experimentos em larga escala. O exemplo mais not\'avel \'e provavelmente a precess\~ao da \'orbita de Merc\'urio em torno do Sol. Equanto a previs\~ao obtida pela RG \'e t\~ao precisa quanto se pode medir, a previs\~ao da Mec\^anica Newtoniana possui mais de 1\% de imprecis\~ao.

Duas no\c c\~oes de massa (ou energia) bem estabelecidas em RG s\~ao a no\c c\~ao de densidade local de energia em um ponto e a no\c c\~ao de massa total de um espa\c co-tempo assintoticamente plano (isto \'e, um espa\c co-tempo que modela um sistema gravitacional isolado). A no\c c\~ao de massa total foi introduzida em 1959 pelos f\'{\i}sicos Arnowitt, Deser e Misner e \'e conhecida hoje pelo nome de massa ADM. Tal no\c c\~ao foi derivada da chamada formula\c c\~ao hamiltoniana da RG.
Apesar da terminologia empregada, a massa ADM de um espa\c co-tempo n\~ao \'e a integral da densidade local de energia. Al\'em disso, quando olhamos para as defini\c c\~oes formais, n\~ao \'e claro que haja uma liga\c c\~ao entre estas duas no\c c\~oes de energia.

Dois testes fundamentais para RG como uma teoria f\'{\i}sica s\~ao o Teorema da Massa Positiva (TMP) e a Conjectura de Penrose (CP). O TMP diz que se a densidade local de energia \'e n\~ao-negativa, ent\~ao a massa ADM tamb\'em \'e n\~ao-negativa. A CP, formulada em 1973 pelo f\'{\i}sico Roger Penrose \cite{penrose}, o qual deu um argumento heur\'{\i}stico para a validade da mesma, diz que se a densidade local de energia \'e n\~ao-negativa, ent\~ao a massa ADM de uma espa\c co-tempo que possui algum buraco-negro \'e limitada inferiormente por um termo que depende da \'area do chamado horizonte aparente do buraco-negro.

Tanto o TMP como a CP possuem um caso particular conhecido como a ``vers\~ao riemanniana"  de tais teoremas. Neste caso especial, tais teoremas se traduzem em lindos teoremas de geometria riemanniana.

A vers\~ao riemanniana do TMP foi verificada por Schoen e Yau em 1979 \cite{schoen-yau1} para espa\c cos-tempo de dimens\~ao menor ou igual a oito. Logo depois, em 1981, a vers\~ao geral do TMP (para espa\c cos-tempo de dimens\~ao menor ou igual a oito) foi reduzida \`a vers\~ao riemanniana, tamb\'em por Schoen e Yau \cite{schoen-yau2}. Tamb\'em em 1981, Witten \cite{witten} exibiu uma prova (para a vers\~ao geral) que vale em qualquer dimens\~ao, desde que o espa\c co tempo satisfa\c ca uma certa condi\c c\~ao topol\'ogica. A prova dada por Witten foi revolucion\'aria, pois utilizou, de forma muito elegante, entes matem\'aticos chamados {\it spinors}. 

J\'a a vers\~ao riemanniana da CP foi verificada, em 2001, por Huisken e Ilmanen \cite{huisken-ilmanen} para espa\c cos-tempo de dimens\~ao 4 e um buraco-negro. Tamb\'em em 2001, Bray \cite{bray} publicou uma prova que vale para espa\c cos-tempo de dimens\~ao quatro e um n\'umero qualquer (finito) de buracos-negros. A prova dada por Bray foi ent\~ao estendida em 2008, por Bray e Lee \cite{bray-lee}, para espa\c cos-tempo de dimens\~ao menor ou igual a oito.

A vers\~ao geral da CP est\'a completamente aberta. 
%Vale salientar que nem ao menos um consenso sobre o enunciado correto desta  conjectura existe.
 Para espa\c cos-tempo de dimens\~ao maior que oito, at\'e mesmo a vers\~ao riemanniana da CP est\'a aberta.

\subsection*{Enunciados Precisos}

Iremos agora descrever com mais precis\~ao o Teorema da Massa Positiva (TMP) e a Conjectura de Penrose (CP). Para deixar a exposi\c c\~ao mais simples, consideraremos espa\c cos-tempo de dimens\~ao quatro, mas pode-se, sem grandes dificuldades, enunciar os mesmos problemas em dimens\~oes maiores.

Seja $(\mathcal{N}^{4},g)$ uma variedade de Lorentz cuja m\'etrica satisfaz \`as equa\c c\~oes de Einstein
$$
\mathrm{Ric}_g - \frac{1}{2}\mathrm{R}_g = 8\pi T,
$$
onde $T$ \'e o chamado tensor energia-momento e estamos assumindo que $g$ tem signatura $(-,+,+,+)$.

Uma hipersuperf\'{\i}cie tipo-espa\c co $(M^3,h,k)$ de $(\mathcal{N},g)$ (com um fim) diz-se assintoticamente plana se, fora de um conjunto compacto, $M$ \'e difeomorfa a $\mathbb{R}^3 - \overline{B}_1(0)$ e o sistema de coordenadas induzido pelo difeomorfismo satisfaz \`as seguintes condi\c c\~oes de decaimento:
$$
\partial^{\alpha}(h_{ij} - \delta_{ij}) = O(|x|^{-1-\alpha}), \ \ |\alpha| \leq 2; \ \
\partial^{\beta}(k_{ij} - \delta_{ij}) = O(|x|^{-2-\beta}), \ \ |\beta| \leq 1.
$$
Aqui $h$ \'e a m\'etrica riemanniana induzida em $M^3$ e $k$ \'e a sua segunda forma fundamental.

Se denotarmos por $n$ o campo unit\'ario normal a $M$ (que aponta para o futuro), ent\~ao $\mu := T(n,n)$ denota a densidade de energia e $J:= T(n,\cdot)$ denota a chamada densidade de momento. As equa\c c\~oes de Gauss e Codazzi, respectivamente, nos d\~ao
$$ 2 \mu = \mathrm{R}_h + (\mathrm{tr}_hk)^2 -||k||^2, \ \ \ J_i = \nabla^j(k_{ij} - (\mathrm{tr}_hk)h_{ij}), $$
onde $\mathrm{R}_h$ denota a curvatura escalar da m\'etrica $h$. Estas s\~ao as chamadas equa\c c\~oes de v\'{\i}nculo. Uma tripla $(M,h,k)$ como a descrita acima \'e chamada um dado inicial de Cauchy.

Seja $(M,h,k)$ um dado inicial de Cauchy que satisfaz \`a condi\c c\~ao da energia dominante, isto \'e,
$$ \mu \geq |J| .$$
Assuma tamb\'em que $(M,h,k)$ \'e assintoticamente plana, que $\int_M |R_h| < \infty$ e que $\int_M |J| < \infty$.

Sob tais hip\'oteses, definimos a \it{energia total} \rm (ou \it{massa}\rm) do dado inicial de Cauchy $(M,h,k)$ como 
$$
E := \frac{1}{16\pi} \lim_{r \to \infty} \int_{S_r} (g_{ij,i} - g_{ii,j})\nu_j dS_r,
$$
onde $S_r$ \'e a esfera coordenada de raio $r$, $dS_r$ sua forma de volume e $\nu$ o campo unit\'ario normal a $S_r$ que aponta para fora. Definimos tamb\'em o \it momento linear \rm de $(M,h,k)$ como
$$
P_i := \frac{1}{8 \pi} \lim_{r \to \infty} \int_{S_r} (h_{ij} - \mathrm{tr}_{\delta}(h)\delta_{ij}) \nu_j dS_r.
$$
Sabe-se que $E$ e $P_i$ est\~ao bem definidos, isto \'e, n\~ao dependem das coordenadas escolhidas para calcul\'a-los.

Estamos agora em posi\c c\~ao de enunciar a vers\~ao geral do TMP.

\begin{teorema}[TMP] Seja $(M,h,k)$ um dado inicial de Cauchy. Se $(M,h,k)$ \'e assintoticamente plana e satisfaz \`a condi\c c\~ao da energia dominante $(\mu \geq |J|)$, ent\~ao
$$ E \geq |P|.$$
Ademais, se a igualdade ocorre, ent\~ao $(M,h,k)$ \'e uma hipersuperf\'{\i}cie tipo-espa\c co do espa\c co de Minkowski, com m\'etrica induzida $h$ e segunda forma fundamental $k$.
\end{teorema}
Um caso especial importante deste teorema \'e o caso em que $h=0$. Este caso \'e chamado de ``vers\~ao riemanniana" do TMP.

\begin{teorema}
Se $(M,h)$ \'e assintoticamente plana e $R_h \geq 0$, ent\~ao $E \geq 0$. Al\'em disso, se $E=0$, ent\~ao $(M,h)$ \'e isom\'etrica ao espa\c co euclideano munido da m\'etrica can\^onica.
\end{teorema}

Para finalizar, enunciemos a ``vers\~ao riemanniana" da CP.

\begin{teorema}
Suponha que a variedade riemanniana $(M^3,h)$ \'e completa, assintoticamente plana, com curvatura escalar $R_h \geq 0$ e massa $m$. %Se $\Sigma^2$ \'e uma superf\'{\i}cie m\'{\i}nima de $(M^3,h)$, ent\~ao
Tem-se
$$ m \geq \sqrt{\frac{A}{16\pi}},$$
onde $A$ \'e a \'area da chamada envolt\'oria m\'{\i}nima ``outermost" $\ \Sigma = \partial U^3$ de $(M^3,h)$. Al\'em disso, se a igualdade ocorre, ent\~ao $(M - U,h)$ \'e isom\'etrica ao espa\c co de Schwarzschild
$$\left(\mathbb{R}^3 - B_{m/2}(0), (1 + \frac{m}{2r})^4(dx^2+dy^2+dz^2)\right).$$
\end{teorema}
%%%%%%%%%

\section*{Publica\c c\~oes e pr\'e-publica\c c\~oes}

Listamos nesta sess\~ao trabalhos conclu\'{\i}dos relacionados ao tema deste projeto. Os artigos de 1 a 4, descritos a seguir, s\~ao de autoria de Levi Lopes de Lima e Frederico Vale Gir\~ao.
\\ \\
1.(\cite{levi-fred1}) \it The ADM mass of asymptotically flat hypersurfaces. \rm  Trans. Amer. Math. Soc. 367 (2015), no. 9, 6247-6266. 
\\ \\
Abstract: We provide integral formulae for the ADM mass of asymptotically flat hypersurfaces in Riemannian manifolds with a certain warped product structure in a neighborhood of infinity, thus extending Lam's recent results on Euclidean graphs to this broader context. As applications we exhibit, in any dimension, new classes of manifolds for which versions of the Positive Mass and Riemannian Penrose inequalities hold and discuss a notion of quasi-local mass in this setting. The proof explores a novel connection between the co-vector defining the ADM mass of a hypersurface as above and the Newton tensor associated to its shape operator, which takes place in the presence of an ambient Killing field. 
\\ \\
2.(\cite{levi-fred2}) \it Positive mass and Penrose type inequalities for asymptotically hyperbolic hypersurfaces. \rm Gen. Relativity Gravitation 47 (2015), no. 3, Art. 23, 20 pp.
\\ \\
Abstract: We establish versions of the Positive Mass and Penrose inequalities for a class of asymptotically hyperbolic hypersurfaces. In particular, under the usual dominant energy condition, we prove in all dimensions $n\geq 3$ an optimal Penrose inequality for certain graphs in hyperbolic space $\mathbb H^{n+1}$ whose boundary has constant mean curvature $n-1$. 
\\ \\
3.(\cite{levi-fred4}) \it An Alexandrov-Fenchel-type inequality in hyperbolic space with an application to a Penrose inequality. \rm Ann. Henri Poincar\'e (a ser publicado). Dispon\'{\i}vel em http://link.springer.com/article/10.1007/s00023-015-0414-0.
\\ \\
Abstract: We prove a sharp Alexandrov-Fenchel-type inequality for star-shaped, strictly mean convex hypersurfaces in hyperbolic $n$-space, $n \geq 3$. The argument uses two new monotone quantities along the inverse mean curvature flow. As an application we establish, in any dimension, an optimal Penrose inequality for asymptotically hyperbolic graphs carrying a minimal horizon, with the equality occurring if and only if the graph is an anti-de Sitter-Schwarzschild solution. This sharpens previous results by Dahl-Gicquaud-Sakovich and settles, for this class of initial data sets, the conjectured Penrose inequality for time-symmetric space–times with negative cosmological constant. We also explain how our methods can be easily adapted to derive an optimal Penrose inequality for asymptotically locally hyperbolic graphs in any dimension $n \geq 3$. When the horizon has the topology of a compact surface of genus at least one, this provides an affirmative answer, for this class of initial data sets, to a question posed by Gibbons, Chru\'sciel and Simon on the validity of a Penrose-type inequality for exotic black holes.
\\ \\
4.(\cite{levi-fred3}) \it A rigidity result for the graph case of the Penrose inequality. \rm Preprint \tt arXiv \rm 1205.1132.
\\ \\
Abstract: In this note we prove a global rigidity result for asymptotically flat, scalar flat Euclidean hypersurfaces with a minimal horizon lying in a hyperplane, under a natural ellipticity condition. As a consequence we obtain, in the context of the Riemannian Penrose conjecture, a local rigidity result for the family of exterior Schwarzschild solutions (viewed as graphs in Euclidean space). 
\\ 


O artigo a seguir \'e de autoria de Levi Lopes de Lima, Frederico Gir\~ao, Weslley Loz\'orio e Juscelino Silva.
\\ \\
5.(\cite{levi-fred-weslley-juscelino}) \it Penrose inequalities and a positive mass theorem for charged black holes in higher dimension. \rm Preprint \tt arXiv \rm 1401.0945.
\\ \\
Abstract: We use the inverse mean curvature flow to establish Penrose-type inequalities for time-symmetric Einstein-Maxwell initial data sets which can be suitably embedded as a hypersurface in Euclidean space $\mathbb R^{n+1}$, $n\geq 3$. In particular, we prove a positive mass theorem for this class of charged black holes. As an application we show that the conjectured upper bound for the area in terms of the mass and the charge, which in dimension $n=3$ is relevant in connection with the Cosmic Censorship Conjecture, always holds under the natural assumption that the horizon is stable as a minimal hypersurface.
\\

O artigo a seguir \'e de autoria de Ezequiel Barbosa, Levi Lopes de Lima e Frederico Gir\~ao.
\\ \\
6.(\cite{ezequiel-levi-fred}) \it On the limiting behavior of the Brown-York quasi-local mass in asymptotically hyperbolic manifolds. \rm Preprint \tt arXiv \rm 1407.0660.
\\ \\
Abstract: We show that the limit at infinity of the vector-valued Brown-York-type quasi-local mass along any coordinate exhaustion of an asymptotically hyperbolic $3$-manifold satisfying the relevant energy condition on the scalar curvature has the conjectured causal character. Our proof uses spinors and relies on a Witten-type formula expressing the asymptotic limit of this quasi-local mass as a bulk integral which manifestly has the right sign under the above assumptions. In the spirit of recent work by Hijazi, Montiel and Raulot, we also provide another proof of this result which uses the theory of boundary value problems for Dirac operators on compact domains to show that a certain quasi-local mass, which converges to the Brown-York mass in the asymptotic limit, has the expected causal character under suitable geometric assumptions.
\\

O artigo a seguir \'e de autoria de Frederico Gir\~ao.
\\ \\
7.(\cite{fred}) \it An eigenvalue comparison theorem for the Dirac operator on surfaces. \rm Submetido para publica\c c\~ao.
\\ \\
Abstract: We use the Ricci flow on surfaces to give upper and lower estimates for the smallest eigenvalue of the square of the Dirac operator of a surface $(M,g)$ of negative Euler characteristic. The estimates depend on simple geometric data of $(M,g)$ and on the smallest eigenvalue of the square of the Dirac operator corresponding to the unique constant curvature metric on the conformal class of $g$ whose volume equals that of $g$.
\\

Dentre os trabalhos acima descritos, gostar\'{\i}amos de destacar o artigo \cite{levi-fred4}, o qual j\'a foi citado por pelo menos 18 outras pr\'e-publica\c c\~oes: \\ \tt arXiv:1407.0660, arXiv:1406.1768, arXiv:1405.4518, arXiv:1403.6108, arXiv:1402.4317, arXiv:1401.0945, arXiv:1308.5544, arXiv:1307.5764, arXiv:1307.4239, arXiv:1201.3321, arXiv:1305.2805, arXiv:1304.7887, arXiv:1304.1674, arXiv:1304.1417, arXiv:1303.1714, arXiv:1212.4218, arXiv:1211.4109, arXiv:1205.2061. \rm

\section*{Problemas de Pesquisa}
Nesta se\c c\~ao descreveremos alguns problemas de pesquisa relacionados ao Teorema da Massa Positiva (TMP) e \`a Conjectura de Penrose (CP).
%%%%%
\\  

Apesar dos resultados satisfat\'orios obtidos para gr\'aficos \cite{lam,levi-fred1,levi-fred2,levi-fred4}, ainda n\~ao se conseguiu uma demonstra\c c\~ao da CP no caso de hipersuperf\'{\i}cies, mergulhadas em formas espaciais, que n\~ao podem ser realizadas como gr\'aficos.
\\ \\
\bf{Problema 1.} \rm Demonstrar a CP para hipersuperf\'{\i}cies do espa\c co euclideano $(\mathbb{R}^{n+1}, \delta)$.
\\ \\ \\
\bf{Problema 2.} \rm Demonstrar a CP (vers\~oes hiperb\'olicas) para hipersuperf\'{\i}cies de espa\c co hiperb\'olico $(\mathbb{H}^{n+1},g_{-1})$.
\\  

No caso em que a variedade riemanniana em quest\~ao \'e conformemente plana, um argumento simples demonstra o TMP. Apesar disto, ainda n\~ao se conseguiu uma demonstra\c c\~ao da CP neste contexto. Alguns trabalhos nesta dire\c c\~ao s\~ao \cite{schwartz, freire-schwartz}.
\\ \\
\bf{Problema 3.} \rm Demonstrar a CP para m\'etricas conformemente planas.
\\  

Logo ap\'os Schoen e Yau publicarem uma demonstra\c c\~ao do TMP, Witten \cite{witten} publicou uma bel\'{\i}ssima demonstra\c c\~ao utilizando entes geom\'etricos chamados \it spinors. \rm Entretanto, n\~ao se conseguiu, at\'e o momento, uma demonstra\c c\~ao da CP utilizando tal t\'ecnica. Um excelente trabalho nesta dire\c c\~ao \'e o artigo \cite{herzlich}.
\\ \\
\bf{Problema 4.} \rm Encontrar uma demonstra\c c\~ao da CP utizando \it spinors. \rm
\\ 

Uma teoria gravitacional similar \`a teoria de Einstein \'e a chamada teoria gravitacional de Lovelock. Nesta teoria, as equa\c c\~oes de campo no v\'acuo envolvem um tensor que depende polinomialmente da curvatura. Recentemente, meu co-autor Levi Lopes de Lima obteve, em trabalhos em parceria com T. Caula e N. Santos, resultados de rigidez e deforma\c c\~ao para certos invariantes derivados dos tensores de Lovelock \cite{levi-caula-newton,levi-newton}. Em \cite{ge-wang-wu}, Ge, Wang and Wu introduziram uma no\c c\~ao de massa para solu\c c\~oes das equa\c c\~oes de campo da teoria gravitacional de Lovelock. Em \cite{ge-wang-wu} e em \cite{ge-wang-wu2}, alguns casos particulares do TMP e da CP foram demonstrados.
\\ \\
\bf{Problema 5.} \rm Demonstrar o TMP e a CP no contexto da gravidade de Lovelock.
\\ 

No caso em que a constante cosmol\'ogia \'e positiva existe, na literatura f\'{\i}sica, uma no\c c\~ao de massa, como tamb\'em uma vers\~ao do TMP. Ainda n\~ao h\'a, por\'em, uma vers\~ao da CP neste caso. Observamos que uma formula\c c\~ao apropriada da CP neste contexto representaria uma reabilita\c c\~ao da Conjectura de Min-On, que em sua formula\c c\~ao original foi recentemente inviabilizada por Brendle-Marques-Neves \cite{brendle-marques-neves}.
\\ \\
\bf{Problema 6.} \rm Encontrar uma formula\c c\~ao apropriada da CP no caso em que a constante cosmol\'ogica \'e positiva e demonstrar tal formula\c c\~ao. 


\section*{Objetivos}
\begin{itemize}
\item Resolver e publicar em peri\'odicos de renome internacional problemas relacionados \`a no\c c\~ao de massa (ou energia), como os problemas listados na se\c c\~ao ``Problemas de Pesquisa".
\item Iniciar colabora\c c\~oes nacionais e internacionais e publicar os resultados em peri\'odicos renomados.
\item Difundir o tema de pesquisa deste projeto junto ao Departamento de Matem\'atica da UFC, participando de semin\'arios semanais e orientando alunos de mestrado e doutorado em temas relacionados.
\item Divulgar os resultados obtidos atrav\'es de visitas a departamentos de Matem\'atica no Brasil e no exterior e atrav\'es de participa\c c\~oes em confer\^encias nacionais e internacionais. Tais atividades facilitar\~ao a colabora\c c\~ao com pesquisadores de outras institui\c c\~oes.
\end{itemize}

\section*{Metodologia}

Eu e meu co-autores usaremos a metodologia t\'{\i}pica de pesquisa em Matem\'atica (pura), isto \'e, trabalharemos isoladamente em um problema de pesquisa escolhido, estudando a bibliografia relacionada \`aquele problema e tentanto avan\c car em dire\c c\~ao a uma solu\c c\~ao do mesmo. Durante este processo, nos reuniremos periodicamente para a troca de id\'eias e de resultados obtidos at\'e aquele momento.

Eventualmente pode haver a necessidade de utiliza\c c\~ao de um \it software \rm de computa\c c\~ao simb\'olica, como o \it Matlab,   \rm por exemplo, para efetuar alguns dos c\'alculos mais complexos.


\begin{thebibliography}{99}
\bibitem[BdLG]{ezequiel-levi-fred} E. Barbosa, L. L. de Lima, F. Gir\~ao, {\it On the limiting behavior of the Brown-York quasi-local mass in asymptotically hyperbolic manifolds,} {\tt arXiv:1407.0660}.

\bibitem[B]{bray} H. Bray, {\it Proof of the Riemannian Penrose inequality using the positive mass theorem,} J. Differential Geom. 59  (2001), 177-297.

%\bibitem{anderson2} M. T. Anderson, {\it Topics in conformally compact einstein metrics,} Perspectives in Riemannian Geometry, 1-26, CRM Proc. Lecture Notes, {\bf 40}, Amer. Math. Soc., Providence, RI, 2006. 

%\bibitem{bar} C. B\"ar, {\it Extrinsic bounds for eigenvalues of the Dirac operator,} Ann. Global Anal. Geom. {\bf 16} (1998), no. 6, 573 - 596.

%\bibitem{b-g} J. P. Bourguignon, P. Gauduchon {\it Spineurs, op\'erateus de Dirac, et variations de m\'etricques,}
% Comm. Math. Phys. {\bf 144} (1992), no. 3, 581-599.

%\bibitem{fefferman-graham} C. Fefferman and C. R. Graham, {\it Conformal invariants,} \'Elie Cartan et les Math\'ematiques d'Aujourd'hui, Ast\'erisque, 1985, Num\'ero hors s\'erie, Soc. Math. France, Paris, pp.95-116.

%\bibitem{girao1} F. Gir\~ao, {\it Orbifold degeneration of conformally compact Einstein metrics,} PhD thesis, SUNY at Stony Brook (2010), em prepara\c c\~ao.

%\bibitem{girao2} F. Gir\~ao, {\it A comparison theorem for the first eigenvalue of the Dirac operator on surfaces,} em prepara\c c\~ao.

%\bibitem{gromov-lawson1} M. Gromov and H. B. Lawson Jr., {\it Spin and scalar curvature in the presence of a fundamental group. I,} Ann. of Math. (2) {\bf 111} (1980), no. 2, 209 - 230.

%\bibitem{gromov-lawson2} M. Gromov and H. B. Lawson Jr., {\it The classification of simply connected manifolds with positive scalar curvature,} Ann. of Math. (2) {\bf 111} (1980), no. 3, 423-434. 

\bibitem[BL]{bray-lee} H. Bray, D. Lee, {\it On the Riemannian Penrose inequality in dimensions less than 8,} Duke Math. J. 148 (2009), no. 1, 81-106.

\bibitem[BMN]{brendle-marques-neves}  S. Brendle, F.  Marques, A.Neves, {\it Deformations of the hemisphere that increase scalar curvature,} {Invent. Math.} 185 (2011), no. 1, 175-197.

\bibitem[CdLS]{levi-caula-newton} T. Caula, L. L. de Lima, N. Santos,
{\it Deformation and rigidity results for the 2k-Ricci tensor and the 2k-Gauss-Bonnet curvature,} Math. Nachr. 286 (2013), no. 17-18, 1752-1777.

\bibitem[dLG1]{levi-fred1} L. L. de Lima, F. Gir\~ao, {\it The ADM mass of asymptotically flat hypersurfaces,} Trans. Amer. Math. Soc. 367 (2015), no. 9, 6247-6266.

\bibitem[dLG2]{levi-fred2} L. L. de Lima, F. Gir\~ao, {\it Positive mass and Penrose type inequalities for asymptotically hyperbolic hypersurfaces}, Gen. Relativity Gravitation 47 (2015), no. 3, Art. 23, 20 pp.

\bibitem[dLG3]{levi-fred3}  L. L. de Lima, F. Gir\~ao, {\it A rigidity result for the graph case of the Penrose inequality,} {\tt arXiv:1205.1132}.

\bibitem[dLG4]{levi-fred4}  L. L. de Lima, F. Gir\~ao, {\it An Alexandrov-Fenchel inequality in hyperbolic space with an application to a Penrose inequality,} Ann. Henri Poincar\'e (a ser publicado). Dispon\'{\i}vel em http://link.springer.com/article/10.1007/s00023-015-0414-0.

%\bibitem[dLG5]{levi-fred5}  L. L. de Lima, F. Gir\~ao, {\it The Penrose inequality for locally asymptotically hyperbolic manifolds,} em preparação.

\bibitem[dLS]{levi-newton} L. L. de Lima, N. Santos, {\it Deformations of 2k-Einstein structures,} { J. Geom. Phys.} 60 (2010), no. 9, 1279-1287.

\bibitem[dLGLS]{levi-fred-weslley-juscelino} L. L. de Lima, F. Gir\~ao, J. Silva, W. Loz\'orio, {\it Penrose inequalities and a positive mass theorem for charged black holes in higher dimension,} {\tt arXiv:1401.0945}.

\bibitem[GWW]{ge-wang-wu} Y. Ge, G. Wang, J. Wu,
{\it A new mass for asymptotically flat manifolds,} Adv. Math. 266 (2014), 84-119.

\bibitem[GWW2]{ge-wang-wu2} Y. Ge, G. Wang, J. Wu,
{\it The Gauss-Bonnet-Chern mass of conformally flat manifolds.} Math. Res. Not. IMRN 2014, no. 17, 4855-4878.

\bibitem[G]{fred} F. Gir\~ao, {\it An eigenvalue comparison theorem for the Dirac operator on surfaces.} Submetido para publica\c c\~ao.

%\bibitem[HE]{hawking-ellis} S. W. Hawking, G. F. R. Ellis, {\it The large scale structure of space-time,} Cambridge Monographs in Mathematical Physics, No. 1.

\bibitem[H]{herzlich} M. Herzlich, {\it A Penrose-like inequality for the mass of Riemannian asymptotically flat manifolds,}  Comm. Math. Phys. 188 (1997), no. 1, 121-133.

\bibitem[HI]{huisken-ilmanen} G. Huisken, T. Ilmanen, {\it Inverve mean curvature flow and the Riemannian Penrose Inequality,} J. Differential Geom. { 59} (2001), no. 3, 353-437.

%\bibitem{huisken} G. Huisken, {\it Flow by mean curvature of convex surfaces into spheres,} J. Differential. Geom. {\bf 20} (1984), no. 1, 237 - 266.

%\bibitem{lee-sesum} N. Lee, N. Sesum, {\it On the extension of the Ricci flow,} Preprint, 2010; {\tt arXiv:1005.1220v1}.

%\bibitem{maldacena} J. Maldacena, {\it The large N limit of superconformal field theories and supergravity,} Adv. Theor. Math. Phys. {\bf 2} (1998), 231-252.

%\bibitem{singer-mazzeo} R. Mazzeo, M. Singer {\it some remarks on conic degeneration and bending of Poincar\'e-Einstein metrics,} Preprint, 2007; {\tt arXiv:0709.1498v1}.

\bibitem[L]{lam} M.-K. G. Lam, {\it The graphs cases of the Riemannian Positive Mass and Penrose Inequalities in All Dimensions,} {\tt arXiv:1102.5749}.

%\bibitem[M]{mars} M. Mars, {\it Present status of the Penrose inequality,} Classical Quantum Gravity 26 (2009), no. 19, 193001.

\bibitem[N]{neves} Neves, A., {\it Insufficient convergence of inverse mean curvature flow on asymptotically hyperbolic manifolds,} {J. Differential Geom.} 84 (2010), no. 1, 191-229.

%\bibitem{schoen-yau_2} R. Schoen, S.-T. Yau, {\it Proof of the positive mass theorem II,} Comm. Math. Phys. 79 (1981), 231-60.



\bibitem[P]{penrose} R. Penrose {\it Naked singularities,} Ann. New York Acad. Sci. 224 (1973), 125-34.

\bibitem[SY1]{schoen-yau1} R. Schoen, S.-T. Yau, {\it On the proof of the positive mass conjecture in general relativity,} Comm. Math. Phys. 65 (1979),45-76.

\bibitem[ST2]{schoen-yau2} R. Schoen, S.-T. Yau, {\it Proof of the positive mass theorem II,} Comm. Math. Phys. 79 (1981), 231-60.

\bibitem[S]{schwartz} F. Schwartz, {\it A volumetric Penrose inequality for conformally flat manifolds,} Ann. Henri Poincar\'e 12 (2011), no. 1, 67–76.

\bibitem[SF]{freire-schwartz} A. Freire, F. Schwartz, {\it Mass-capacity inequalities for conformally flat manifolds with boundary,} Comm. Partial Differential Equations 39 (2014), no. 1, 98-119.

\bibitem[W]{witten} E. Witten {\it A new proof of the positive energy theorem,} Comm. Math. Phys. { 80} (1981), no. 3, 381-402.

\end{thebibliography} 



\end{document}